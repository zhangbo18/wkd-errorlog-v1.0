import request from '@/utils/request'

export function getList(data) {
  return request({
    url: '/errorLog/getList',
    method: 'post',
    data
  })
}

export function getexcel(query) {
  return request({
    url: '/exp/example.xlsx',
    method: 'get',
    params: query
  })
}

export function getRankList(data) {
  return request({
    url: '/errorLog/getRank',
    method: 'post',
    data
  })
}
// export function fetchArticle(id) {
//   return request({
//     url: '/article/detail',
//     method: 'get',
//     params: { id }
//   })
// }

export function deleteItem(data) {
  return request({
    url: '/user/deleteLog',
    method: 'post',
    data
  })
}

export function createItem(data) {
  return request({
    url: 'item/intoItem',
    method: 'post',
    data
  })
}

export function updateItem(data) {
  return request({
    url: 'item/updItem',
    method: 'post',
    data
  })
}

export function changehot(data) {
  return request({
    url: '/item/ishot',
    method: 'post',
    data
  })
}
